License
=======

The following directories and their contents copyright © Harry Mills:

_posts/
lib/images/

Everything else is MIT Licensed.  Attribution is always appreciated.
